#include "model.h"
#include "constants.h"

namespace cdfl {

Chapter::Chapter(unsigned int index_, const std::string& title_):
        index(index_),
        title(title_) {}

std::ostream& operator<< (std::ostream& out, const Chapter& obj) {
    out << "{index: " << obj.index << ", title: " << obj.title << "}";
    return out;
}

ModeSelection::ModeSelection(const wxString& mode_, Chapter ch_, const wxString& hint_, const wxString& dir_, const wxString& eval_):
        mode(mode_),
        chapter(ch_),
        hint(hint_),
        direction(dir_),
        evaluate(eval_) {}

std::ostream& operator<< (std::ostream& out, const ModeSelection& obj) {
    out << "{mode: " << obj.mode << ", chapter: " << obj.chapter << ", hint: " << obj.hint << ", direction: " << obj.direction << ", evaluation: " << obj.evaluate << "}";
    return out;
}

Card::Card(const wxString& _front, const wxString& _back, const wxString& _hint, bool _correct):
        front(_front),
        back(_back),
        hint(_hint),
        correct(_correct) {}

std::ostream& operator<< (std::ostream& out, const Card& obj) {
    out << "{front: " << obj.front << ", back: " << obj.back << ", hint: " << obj.hint << ", correct: " << obj.correct << "}";
    return out;
}

}