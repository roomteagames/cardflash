#include "game.h"
#include "utils.h"
#include "constants.h"

#include <wx/wx.h>
#include <algorithm>
#include <random>
#include <chrono>
#include <sstream>
#include <iomanip>

// temp
#include <iostream>

namespace cdfl {

Game::Game(const std::string& resource_path) {
    resource = utils::ReadJSONFile(resource_path);
    create_chapters_list();
}

Game::~Game() {
    delete[] chapters;
}

void Game::ClearCards() {
    cards.clear();
}

void Game::AppendCards() {
    // Populate cards vector
    if (settings.mode == MODE_FULL) {
        for (auto& ch : resource["chapters"].GetArray()) {
            for (auto& entry : ch["cards"].GetArray()) {
                const auto& entry_arr = entry.GetArray();
                
                auto front = entry_arr[0].GetString();
                auto back = entry_arr[1].GetString();
                auto hint = entry_arr[2].GetString();
                
                cards.emplace_back(front, back, hint);
            }
        }
    } else {
        auto index = settings.chapter.index;
        std::cout << "---INDEX: " << index << std::endl;
        auto& ch = resource["chapters"].GetArray()[index];
        for (auto& entry : ch["cards"].GetArray()) {
            const auto& entry_arr = entry.GetArray();
            
            auto front = entry_arr[0].GetString();
            auto back = entry_arr[1].GetString();
            auto hint = entry_arr[2].GetString();
            
            cards.emplace_back(front, back, hint);
        }
    }
}

void Game::ShuffleCards() {
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::shuffle(cards.begin(), cards.end(), std::default_random_engine(seed));
}

void Game::Setup() {
    card_index = 0;
    ClearCards();
    AppendCards();
    ShuffleCards();
}

void Game::create_chapters_list() {
    auto& ch = resource["chapters"];
    
    if (ch.IsArray()) {
        // initialize chapters based on resource size
        num_chapters = ch.Size();
        chapters = new wxString[num_chapters];

        // populate chapters based on title
        unsigned short index = 0;
        for (const auto& v : ch.GetArray()) {
            chapters[index] = std::to_string(index + 1) + " - " + v["title"].GetString();
            index++;
        }
    } else {
        error = true;
    }
}

bool Game::AdvanceCard(bool is_correct) {
    if (is_correct) num_correct += 1;
    answer_phase = false;
    std::cout << "NUMBER OF CARDS: " << cards.size() << ", CARD INDEX: " << card_index << std::endl;
    
    if (card_index < cards.size()) {
        current_card = &cards[card_index];
        std::cout << "ADVANCING CARD: " << cards[card_index] << std::endl;
        card_index++;
        return true;
    }
    std::cout << "RAN OUT OF CARDS!" << std::endl;
    return false;
}

wxString Game::GetInfoText() {
    if (current_card != nullptr) {
        std::string side = "Front";
        if (IsBackDisplayed()) {
            side = "Back";
        }
        return wxString(side + "        " + std::to_string(card_index) + "/" + std::to_string(cards.size()));
    }
    return wxString("error");
}

wxString Game::GetChallengeText() {
    // TODO: temp
    if (current_card == nullptr) {
        std::cout << "current card is nullptr" << std::endl;
    }
    if (current_card != nullptr) {
        if (DIR_FRONT_TO_BACK.IsSameAs(settings.direction)) {
            return current_card->front;
        } else {
            return current_card->back;
        }
    }
    return wxString("error");
}

wxString Game::GetAnswerText() {
    if (current_card != nullptr) {
        if (DIR_FRONT_TO_BACK.IsSameAs(settings.direction)) {
            return current_card->back;
        } else {
            return current_card->front;
        }
    }
    return wxString("error");
}

wxString Game::GetLabelText() {
    if (answer_phase) {
        return GetAnswerText();
    }
    return GetChallengeText();
}

wxString Game::GetHintText() {
    if ((IsFrontDisplayed() && HINT_FRONT.IsSameAs(settings.hint)) ||
            (IsBackDisplayed() && HINT_BACK.IsSameAs(settings.hint))) {
        return current_card->hint;
    }
    return wxString("");
}

wxString Game::GetCorrectText() {
    return wxString("Correct: " + std::to_string(num_correct) + " / " + std::to_string(cards.size()));
}

wxString Game::GetPercentText() {
    std::stringstream s;
    s << std::fixed << std::setprecision(1) << GetEvalPercent();
    return wxString("Percent: " + s.str() + "%");
}

bool Game::IsFrontDisplayed() {
    if (DIR_FRONT_TO_BACK.IsSameAs(settings.direction)) {
        return !answer_phase;
    }
    return answer_phase;
}

bool Game::IsBackDisplayed() {
    if (DIR_BACK_TO_FRONT.IsSameAs(settings.direction)) {
        return !answer_phase;
    }
    return answer_phase;
}

void Game::ShowAnswer() {
    answer_phase = true;
    // TODO: do other stuff to show cards -- refresh layout?
}

void Game::FlipCard() {
    answer_phase = !answer_phase;
}

float Game::GetEvalPercent() {
    return 100 * ((1.0 * num_correct) / cards.size());
}

}