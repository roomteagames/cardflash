#include "constants.h"

namespace cdfl {

// Definition of custom event ids
int cfID_Hello = 1;
int cfBUTTON_Start = 10;
int cfBUTTON_Language = 11;
int cfBUTTON_StartGame = 12;
int cfBUTTON_Flip = 13;
int cfBUTTON_Advance = 14;
int cfBUTTON_Correct = 15;
int cfBUTTON_Incorrect = 16;
int cfBUTTON_Done = 17;
int cfSELECT_Mode = 20;
int cfSELECT_Chapter = 21;
int cfSELECT_Direction = 22;
int cfSELECT_Hint = 23;
int cfSELECT_Eval = 24;

// Modes
const wxString MODE_FULL("Full Flash");
const wxString MODE_CHAPTER("Chapter Flash");
const wxString MODE_VOCAB("Vocabulary List");

// Directions
const wxString DIR_FRONT_TO_BACK("Front to Back");
const wxString DIR_BACK_TO_FRONT("Back to Front");

// Toggle
const wxString TOGGLE_OFF("Off");
const wxString TOGGLE_ON("On");

// Hint Type
const wxString HINT_FRONT("Show On Front");
const wxString HINT_BACK("Show On Back");

}