#include "utils.h"

#include <fstream>
#include <sstream>
#include <iostream>

namespace cdfl {

namespace utils {

rapidjson::Document ReadJSONFile(const std::string& filename) {
    rapidjson::Document json;
    
    std::ifstream file(filename);

    if (file.fail()) {
        std::cout << "LOADING FILE FAILED! " << filename << std::endl;
    } else {
        std::cout << "LOADING FILE SUCCESSFUL! " << filename << std::endl; 
        std::stringstream buffer;
        buffer << file.rdbuf();

        json.Parse(buffer.str().c_str());
    }
    return json;
}

}

}