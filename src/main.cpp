#include "main.h"
#include "frame.h"
#include "game.h"
#include "constants.h"

// TEMP
#include <iostream>
#include "utils.h"

// Definition of event table
wxBEGIN_EVENT_TABLE(cdfl::CardFlashFrame, wxFrame)
    EVT_MENU(cdfl::cfID_Hello,           cdfl::CardFlashFrame::OnHello)
    EVT_MENU(wxID_EXIT,                  cdfl::CardFlashFrame::OnExit)
    EVT_MENU(wxID_ABOUT,                 cdfl::CardFlashFrame::OnAbout)
    EVT_BUTTON(cdfl::cfBUTTON_Start,     cdfl::CardFlashFrame::OnStart)
    EVT_BUTTON(cdfl::cfBUTTON_StartGame, cdfl::CardFlashFrame::OnStartGame)
    EVT_BUTTON(cdfl::cfBUTTON_Flip,      cdfl::CardFlashFrame::OnCardFlip)
    EVT_BUTTON(cdfl::cfBUTTON_Advance,   cdfl::CardFlashFrame::OnCardAdvance)
    EVT_BUTTON(cdfl::cfBUTTON_Correct,   cdfl::CardFlashFrame::OnCardCorrect)
    EVT_BUTTON(cdfl::cfBUTTON_Incorrect, cdfl::CardFlashFrame::OnCardAdvance)
    EVT_BUTTON(cdfl::cfBUTTON_Done,      cdfl::CardFlashFrame::OnGameDone)
    EVT_LISTBOX(cdfl::cfSELECT_Mode,     cdfl::CardFlashFrame::OnModeSelection)
    EVT_LISTBOX(cdfl::cfSELECT_Chapter,  cdfl::CardFlashFrame::OnChapterSelection)
    EVT_LISTBOX(cdfl::cfSELECT_Direction,cdfl::CardFlashFrame::OnDirectionSelection)
    EVT_LISTBOX(cdfl::cfSELECT_Hint,     cdfl::CardFlashFrame::OnHintSelection)
    EVT_LISTBOX(cdfl::cfSELECT_Eval,     cdfl::CardFlashFrame::OnEvalSelection)
    EVT_KEY_DOWN(                        cdfl::CardFlashFrame::OnKeyPress)
wxEND_EVENT_TABLE()

// Calls wxWidgets main method
wxIMPLEMENT_APP(cdfl::CardFlashApp);

namespace cdfl {

bool CardFlashApp::OnInit() {
    
    // Create game obj -- CREATE GAME WITHIN FRAME INSTEAD
    /*auto json = utils::ReadJSONFile("res/language.json");
    std::cout << "CH FROM:" << json["from"].GetString() << ", CH TO: " << json["to"].GetString() << std::endl;

    Game game(json);*/

    // TODO: NEXT -- build logic for shuffling vocab list and displaying as flash cards (how to you show answer? how do you advance to next card?)
    // ALSO -- create options to a) FROM -> TO vs TO -> FROM; b) Whether or not to show "assist" (element 3 in vocab item 'list')


    CardFlashFrame* frame = new CardFlashFrame("Hello World", wxPoint(50, 50), "res/language.json");
    frame->SetSize(wxSize(800, 800));
    frame->Show(true);
    return true;
}

}