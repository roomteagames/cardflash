#include "frame.h"
#include "constants.h"
#include "model.h"

#include <iostream>
#include <wx/listbox.h>

#define NUM_MODES 3

namespace cdfl {

CardFlashFrame::CardFlashFrame(const wxString& title, const wxPoint& pos, const std::string& resource_path):
        wxFrame(NULL, wxID_ANY, title, pos),
        game_(resource_path)
{
    wxMenu* menuFile = new wxMenu;
    menuFile->Append(cfID_Hello, "&Hello...\tCtrl-H",
                     "Help string shown in status bar for this menu item");
    menuFile->AppendSeparator();
    menuFile->Append(wxID_EXIT);

    wxMenu* menuHelp = new wxMenu;
    menuHelp->Append(wxID_ABOUT);

    wxMenuBar* menuBar = new wxMenuBar;
    menuBar->Append(menuFile, "&File");
    menuBar->Append(menuHelp, "&Help");

    SetMenuBar(menuBar);
    
    CreateStatusBar();
    SetStatusText("Welcome to wxWidgets!");
    book_ = new wxSimplebook(this, wxID_ANY);

    // Fonts
    title_font_ = wxFont(60, wxFONTFAMILY_ROMAN, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD);
    secondary_font_ = wxFont(30, wxFONTFAMILY_ROMAN, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL);
    
    // Page construction
    BuildStartPage();
    BuildModePage();
    BuildGamePage();
    BuildResultsPage();

    // Add book pages
    book_->AddPage(start_page_, _T("Start Page"), true);
    book_->AddPage(mode_page_, _T("Mode Page"), false);
    book_->AddPage(game_page_, _T("Game Page"), false);
    book_->AddPage(results_page_, _T("Results Page"), false);

    // Book sizer
    wxBoxSizer* book_sizer = new wxBoxSizer(wxVERTICAL);
    book_sizer->Add(book_, 1, wxEXPAND);
    this->SetSizerAndFit(book_sizer);
}

void CardFlashFrame::BuildStartPage() {
    // Start Page
    start_page_ = new wxPanel(book_, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE_HORIZONTAL);
    
    wxPanel* s_title_pane = new wxPanel(start_page_, wxID_ANY);
    s_title_pane->SetBackgroundColour(wxColor(200, 300, 100));
    wxStaticText* s_title = new wxStaticText(s_title_pane, wxID_ANY, wxString("Card Flash"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE_HORIZONTAL);
    s_title->SetFont(title_font_);
    wxBoxSizer* s_title_sizer = new wxBoxSizer(wxHORIZONTAL);
    s_title_sizer->Add(s_title, 1, wxALIGN_CENTER_VERTICAL);
    s_title_pane->SetSizerAndFit(s_title_sizer);
    
    wxButton* s_start_button = new wxButton(start_page_, cfBUTTON_Start, _T("Start"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE_HORIZONTAL);
    //s_start_button->Bind(wxEVT_BUTTON, &CardFlashFrame::OnStart, this);
    wxButton* s_language_button = new wxButton(start_page_, cfBUTTON_Language, _T("Setup Languages"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE_HORIZONTAL);
    //s_language_button->Bind(wxEVT_BUTTON, &CardFlashFrame::OnExit, this);

    wxBoxSizer* title_sizer = new wxBoxSizer(wxVERTICAL);
    title_sizer->Add(s_title_pane, 1, wxEXPAND);
    title_sizer->Add(s_start_button, 1, wxEXPAND);
    title_sizer->Add(s_language_button, 1, wxEXPAND);
    start_page_->SetSizerAndFit(title_sizer);
}

void CardFlashFrame::BuildModePage() {
     // Mode Page
    mode_page_ = new wxPanel(book_, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE_HORIZONTAL);

    wxPanel* title_pane = new wxPanel(mode_page_, wxID_ANY);
    title_pane->SetBackgroundColour(wxColor(200, 300, 100));
    wxStaticText* title = new wxStaticText(title_pane, wxID_ANY, wxString("Select Mode"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE_HORIZONTAL);
    title->SetFont(title_font_);
    wxBoxSizer* title_sizer = new wxBoxSizer(wxHORIZONTAL);
    title_sizer->Add(title, 1, wxALIGN_CENTER_VERTICAL);
    title_pane->SetSizerAndFit(title_sizer);

    // Selection list box, centered horizontally
    wxString modes[NUM_MODES] = {MODE_FULL, MODE_CHAPTER, MODE_VOCAB};
    wxListBox* mode_select = new wxListBox(mode_page_, cfSELECT_Mode, wxDefaultPosition, wxDefaultSize, NUM_MODES, modes, wxLB_SINGLE | wxALIGN_CENTRE_HORIZONTAL);

    // Create chapter select
    // TODO: add logic to load chapter values (needs to be dynamic array or vector)
    chapter_pane_ = new wxPanel(mode_page_, wxID_ANY);
    wxBoxSizer* chapter_sizer = new wxBoxSizer(wxHORIZONTAL);
    wxStaticText* chapter_label = new wxStaticText(chapter_pane_, wxID_ANY, wxString("Select Chapter"), wxDefaultPosition, wxDefaultSize);
    // TODO: set font of chapter_label to something smaller than title font
    // TODO: list of chapters should be populated from input file -- TEMP
    // wxString chapters[5] = {wxString("1"), wxString("2"), wxString("3"), wxString("4"), wxString("5")};
    wxListBox* chapter_select = new wxListBox(chapter_pane_, cfSELECT_Chapter, wxDefaultPosition, wxDefaultSize, game_.num_chapters, game_.chapters, wxLB_SINGLE | wxALIGN_CENTRE_HORIZONTAL);
    chapter_sizer->Add(chapter_label, 1, wxALIGN_CENTER_VERTICAL);
    chapter_sizer->Add(chapter_select, 1, wxALIGN_CENTER_VERTICAL);
    chapter_pane_->SetSizerAndFit(chapter_sizer);
    chapter_pane_->Hide();

    // Create direction dropdown
    direction_pane_ = new wxPanel(mode_page_, wxID_ANY);
    wxBoxSizer* direction_sizer = new wxBoxSizer(wxHORIZONTAL);
    wxStaticText* direction_label = new wxStaticText(direction_pane_, wxID_ANY, wxString("Game Direction"), wxDefaultPosition, wxDefaultSize);
    wxString directions[2] = {DIR_FRONT_TO_BACK, DIR_BACK_TO_FRONT};
    wxListBox* direction_select = new wxListBox(direction_pane_, cfSELECT_Direction, wxDefaultPosition, wxDefaultSize, 2, directions, wxLB_SINGLE | wxALIGN_CENTRE_HORIZONTAL);
    direction_sizer->Add(direction_label, 1, wxALIGN_CENTER_VERTICAL);
    direction_sizer->Add(direction_select, 1, wxALIGN_CENTER_VERTICAL);
    direction_pane_->SetSizerAndFit(direction_sizer);

    // Create Hint dropdown
    hint_pane_ = new wxPanel(mode_page_, wxID_ANY);
    wxBoxSizer* hint_sizer = new wxBoxSizer(wxHORIZONTAL);
    wxStaticText* hint_label = new wxStaticText(hint_pane_, wxID_ANY, wxString("Hints Style"), wxDefaultPosition, wxDefaultSize);
    wxString hints[3] = {TOGGLE_OFF, HINT_FRONT, HINT_BACK};
    wxListBox* hint_select = new wxListBox(hint_pane_, cfSELECT_Hint, wxDefaultPosition, wxDefaultSize, 3, hints, wxLB_SINGLE | wxALIGN_CENTRE_HORIZONTAL);
    hint_sizer->Add(hint_label, 1, wxALIGN_CENTER_VERTICAL);
    hint_sizer->Add(hint_select, 1, wxALIGN_CENTER_VERTICAL);
    hint_pane_->SetSizerAndFit(hint_sizer);

    // Create Evaluate dropdown
    eval_pane_ = new wxPanel(mode_page_, wxID_ANY);
    wxBoxSizer* eval_sizer = new wxBoxSizer(wxHORIZONTAL);
    wxStaticText* eval_label = new wxStaticText(eval_pane_, wxID_ANY, wxString("Enable Evaluate Mode"), wxDefaultPosition, wxDefaultSize);
    wxString evals[3] = {TOGGLE_OFF, TOGGLE_ON};
    wxListBox* eval_select = new wxListBox(eval_pane_, cfSELECT_Eval, wxDefaultPosition, wxDefaultSize, 2, evals, wxLB_SINGLE | wxALIGN_CENTRE_HORIZONTAL);
    eval_sizer->Add(eval_label, 1, wxALIGN_CENTER_VERTICAL);
    eval_sizer->Add(eval_select, 1, wxALIGN_CENTER_VERTICAL);
    eval_pane_->SetSizerAndFit(eval_sizer);

    // Create start button
    wxButton* start_button = new wxButton(mode_page_, cfBUTTON_StartGame, _T("Start Game"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE_HORIZONTAL);

    wxBoxSizer* mode_sizer = new wxBoxSizer(wxVERTICAL);
    mode_sizer->Add(title_pane, 1, wxEXPAND);
    mode_sizer->Add(mode_select, 1, wxEXPAND);
    mode_sizer->Add(chapter_pane_, 1, wxEXPAND | wxRESERVE_SPACE_EVEN_IF_HIDDEN);
    mode_sizer->Add(direction_pane_, 1, wxEXPAND | wxRESERVE_SPACE_EVEN_IF_HIDDEN);
    mode_sizer->Add(hint_pane_, 1, wxEXPAND | wxRESERVE_SPACE_EVEN_IF_HIDDEN);
    mode_sizer->Add(eval_pane_, 1, wxEXPAND | wxRESERVE_SPACE_EVEN_IF_HIDDEN);
    mode_sizer->Add(start_button, 1, wxEXPAND);
    mode_page_->SetSizerAndFit(mode_sizer);

    Chapter default_ch(0, "Title");
    game_.settings.mode = modes[0];
    game_.settings.chapter = default_ch;
    game_.settings.hint = hints[0];
    game_.settings.direction = directions[0];
    game_.settings.evaluate = evals[0];
}

void CardFlashFrame::BuildGamePage() {
    game_page_ = new wxPanel(book_, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE_HORIZONTAL);

    // Show Info banner text
    wxPanel* info_pane = new wxPanel(game_page_, wxID_ANY);
    info_pane->SetBackgroundColour(wxColor(200, 300, 100)); // TODO: change color
    std::cout << "INFO: " << game_.GetInfoText() << std::endl;
    info_text_ = new wxStaticText(info_pane, wxID_ANY, wxString("Info"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE_HORIZONTAL);
    info_text_->SetFont(secondary_font_);
    wxBoxSizer* info_sizer = new wxBoxSizer(wxHORIZONTAL);
    info_sizer->Add(info_text_, 1, wxALIGN_CENTER_VERTICAL);
    info_pane->SetSizerAndFit(info_sizer);

    // Show challenge text
    wxPanel* challenge_pane = new wxPanel(game_page_, wxID_ANY);
    challenge_pane->SetBackgroundColour(wxColor(25, 100, 250)); // TODO: change color
    std::cout << "Challenge: " << game_.GetChallengeText() << std::endl;
    challenge_text_ = new wxStaticText(challenge_pane, wxID_ANY, wxString("Challenge"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE_HORIZONTAL);
    challenge_text_->SetFont(title_font_);
    wxBoxSizer* challenge_sizer = new wxBoxSizer(wxHORIZONTAL);
    challenge_sizer->Add(challenge_text_, 1, wxALIGN_CENTER_VERTICAL);
    challenge_pane->SetSizerAndFit(challenge_sizer);

    // Show hint text
    game_hint_pane_ = new wxPanel(game_page_, wxID_ANY);
    game_hint_pane_->SetBackgroundColour(wxColor(25, 200, 75)); // TODO: change color
    std::cout << "INFO: " << game_.GetHintText() << std::endl;
    hint_text_ = new wxStaticText(game_hint_pane_, wxID_ANY, wxString("Hint"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE_HORIZONTAL);
    hint_text_->SetFont(secondary_font_);
    wxBoxSizer* hint_sizer = new wxBoxSizer(wxHORIZONTAL);
    hint_sizer->Add(hint_text_, 1, wxALIGN_CENTER_VERTICAL);
    game_hint_pane_->SetSizerAndFit(hint_sizer);
    game_hint_pane_->Hide();

    // On screen buttons
    wxButton* flip_button = new wxButton(game_page_, cfBUTTON_Flip, _T("Flip Card"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE_HORIZONTAL);
    advance_button_ = new wxButton(game_page_, cfBUTTON_Advance, _T("Advance Card"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE_HORIZONTAL);

    // Got it right or wrong
    game_eval_pane_ = new wxPanel(game_page_, wxID_ANY);
    wxButton* correct_button = new wxButton(game_eval_pane_, cfBUTTON_Correct, _T("Correct"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE_HORIZONTAL);
    wxButton* incorrect_button = new wxButton(game_eval_pane_, cfBUTTON_Advance, _T("Wrong"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE_HORIZONTAL);
    wxBoxSizer* game_eval_sizer = new wxBoxSizer(wxHORIZONTAL);
    game_eval_sizer->Add(correct_button, 1, wxALIGN_CENTER_VERTICAL);
    game_eval_sizer->Add(incorrect_button, 1, wxALIGN_CENTER_VERTICAL);
    game_eval_pane_->SetSizerAndFit(game_eval_sizer);

    wxBoxSizer* game_sizer = new wxBoxSizer(wxVERTICAL);
    game_sizer->Add(info_pane, 1, wxEXPAND);
    game_sizer->Add(challenge_pane, 4, wxEXPAND);
    game_sizer->Add(game_hint_pane_, 1, wxEXPAND | wxRESERVE_SPACE_EVEN_IF_HIDDEN);
    game_sizer->Add(flip_button, 1, wxEXPAND);
    game_sizer->Add(advance_button_, 1, wxEXPAND);
    game_sizer->Add(game_eval_pane_, 1, wxEXPAND);
    game_page_->SetSizerAndFit(game_sizer);
}

void CardFlashFrame::BuildResultsPage() {
    results_page_ = new wxPanel(book_, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE_HORIZONTAL);

    // Show Results banner text
    wxPanel* title_pane = new wxPanel(results_page_, wxID_ANY);
    title_pane->SetBackgroundColour(wxColor(200, 300, 100)); // TODO: change color
    wxStaticText* title_text = new wxStaticText(title_pane, wxID_ANY, wxString("Results"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE_HORIZONTAL);
    title_text->SetFont(title_font_);
    wxBoxSizer* title_sizer = new wxBoxSizer(wxHORIZONTAL);
    title_sizer->Add(title_text, 1, wxALIGN_CENTER_VERTICAL);
    title_pane->SetSizerAndFit(title_sizer);

    // Correct Answers (out of total)
    wxPanel* correct_pane = new wxPanel(results_page_, wxID_ANY);
    correct_pane->SetBackgroundColour(wxColor(25, 200, 75));
    correct_text_ = new wxStaticText(correct_pane, wxID_ANY, wxString("Correct"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE_HORIZONTAL);
    correct_text_->SetFont(secondary_font_);
    wxBoxSizer* correct_sizer = new wxBoxSizer(wxHORIZONTAL);
    correct_sizer->Add(correct_text_, 1, wxALIGN_CENTER_VERTICAL);
    correct_pane->SetSizerAndFit(correct_sizer);

    // Percent banner
    wxPanel* percent_pane = new wxPanel(results_page_, wxID_ANY);
    percent_pane->SetBackgroundColour(wxColor(*wxWHITE));
    percent_text_ = new wxStaticText(percent_pane, wxID_ANY, wxString("Percent"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE_HORIZONTAL);
    percent_text_->SetFont(secondary_font_);
    percent_text_->SetForegroundColour(wxColor(*wxBLACK));
    wxBoxSizer* percent_sizer = new wxBoxSizer(wxHORIZONTAL);
    percent_sizer->Add(percent_text_, 1, wxALIGN_CENTER_VERTICAL);
    percent_pane->SetSizerAndFit(percent_sizer);

    wxButton* done_button = new wxButton(results_page_, cfBUTTON_Done, _T("Done"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE_HORIZONTAL);

    wxBoxSizer* results_sizer = new wxBoxSizer(wxVERTICAL);
    results_sizer->Add(title_pane, 3, wxEXPAND);
    results_sizer->Add(correct_pane, 1, wxEXPAND);
    results_sizer->Add(percent_pane, 1, wxEXPAND);
    results_sizer->Add(done_button, 1, wxEXPAND);
    results_page_->SetSizerAndFit(results_sizer);
}


void CardFlashFrame::OnStart(wxCommandEvent& event) {
    std::cout << "ON START TRIGGERED" << std::endl;
    book_->SetSelection(1);
}

void CardFlashFrame::OnExit(wxCommandEvent& event) {
    Close(true);
}

void CardFlashFrame::OnAbout(wxCommandEvent& event) {
    wxMessageBox("This is a wxWidgets' Hello World sample",
                 "About Hello World", wxOK | wxICON_INFORMATION);
}

void CardFlashFrame::OnHello(wxCommandEvent& event) {
    wxLogMessage("Hello world from wxWidgets");
}

void CardFlashFrame::OnModeSelection(wxCommandEvent& event) {
    const auto& selection = event.GetString();
    
    // Logic for dynamic menu updates on mode change
    if (!MODE_FULL.IsSameAs(mode_settings_.mode) && MODE_FULL.IsSameAs(selection)) {
        game_.settings.mode = MODE_FULL;
    } else if (!MODE_CHAPTER.IsSameAs(game_.settings.mode) && MODE_CHAPTER.IsSameAs(selection)) {
        game_.settings.mode = MODE_CHAPTER;
    } else if (!MODE_VOCAB.IsSameAs(game_.settings.mode) && MODE_VOCAB.IsSameAs(selection)) {
        game_.settings.mode = MODE_VOCAB;
    }

    // Hide chapter pane for non-chapter modes
    if (!MODE_CHAPTER.IsSameAs(selection)) {
        chapter_pane_->Hide();
    } else {
        chapter_pane_->Show();
    }

    // Hide game-based options for vocab mode
    if (MODE_VOCAB.IsSameAs(selection)) {
        direction_pane_->Hide();
        hint_pane_->Hide();
        eval_pane_->Hide();
    } else {
        direction_pane_->Show();
        hint_pane_->Show();
        eval_pane_->Show();
    }
    mode_page_->Layout();   // causes menu graphics to update
    std::cout << "SELECTION MADE: " << selection << std::endl;
}

void CardFlashFrame::OnChapterSelection(wxCommandEvent& event) {
    game_.settings.chapter = Chapter(event.GetSelection(), event.GetString().ToStdString()); // TODO: how to actually construct a chapter?
}

void CardFlashFrame::OnDirectionSelection(wxCommandEvent& event) {
    game_.settings.direction = event.GetString();
    std::cout << "CHANGED DIRECTION: " << game_.settings.direction << std::endl;
}

void CardFlashFrame::OnHintSelection(wxCommandEvent& event) {
    game_.settings.hint = event.GetString();
    std::cout << "CHANGED HINT: " << game_.settings.hint << std::endl;
}

void CardFlashFrame::OnEvalSelection(wxCommandEvent& event) {
    game_.settings.evaluate = event.GetString();
    std::cout << "CHANGED EVAL: " << game_.settings.evaluate << std::endl;
}

void CardFlashFrame::OnStartGame(wxCommandEvent& event) {

    std::cout << "STARTING GAME: " << game_.settings << std::endl;
    game_.Setup();
    AdvanceCard();
    book_->SetSelection(2);
}

void CardFlashFrame::OnCardFlip(wxCommandEvent& event) {
    std::cout << "FLIPPING CARD" << std::endl;
    game_.FlipCard();
    info_text_->SetLabel(game_.GetInfoText());
    challenge_text_->SetLabel(game_.GetLabelText());
    if (!TOGGLE_OFF.IsSameAs(game_.settings.hint)) {
        game_hint_pane_->Show();
        hint_text_->SetLabel(game_.GetHintText());
    } else {
        game_hint_pane_->Hide();
    }
    game_page_->Layout();
}

void CardFlashFrame::OnCardAdvance(wxCommandEvent& event) {
    std::cout << "ADVANCING CARD" << std::endl;
    AdvanceCard(false);
}

void CardFlashFrame::AdvanceCard(bool is_correct) {
    std::cout << "BEFORE ADVANCE" << std::endl;
    if (game_.AdvanceCard(is_correct)) {
        std::cout << "AFTER ADVANCE" << std::endl;
        info_text_->SetLabel(game_.GetInfoText());
        challenge_text_->SetLabel(game_.GetLabelText());
        if (!TOGGLE_OFF.IsSameAs(game_.settings.hint)) {
            game_hint_pane_->Show();
            hint_text_->SetLabel(game_.GetHintText());
        } else {
            game_hint_pane_->Hide();
        }
    } else {
        HandleCardFinish();
    }

    if (TOGGLE_ON.IsSameAs(game_.settings.evaluate)) {
        advance_button_->Hide();
        game_eval_pane_->Show();
    } else {
        advance_button_->Show();
        game_eval_pane_->Hide();
    }
    game_page_->Layout();
}

void CardFlashFrame::HandleCardFinish() {
    if (TOGGLE_ON.IsSameAs(game_.settings.evaluate)) {
        std::cout << "Entering results page..." << std::endl;
        book_->SetSelection(3);

        // Populate evaluate screen
        correct_text_->SetLabel(game_.GetCorrectText());
        percent_text_->SetLabel(game_.GetPercentText());
        results_page_->Layout();
    } else {
        std::cout << "GOING HOME" << std::endl;
        // Back to home screen? (if not evaluate mode)
        book_->SetSelection(1);
    }
}

void CardFlashFrame::OnCardCorrect(wxCommandEvent& event) {
    std::cout << "CARD ANSWER CORRECT" << std::endl;
    AdvanceCard(true);
}

void CardFlashFrame::OnKeyPress(wxKeyEvent& event) {
    wxChar keyValue = event.GetUnicodeKey();
    if (keyValue != WXK_NONE) {
        std::cout << "KEY PRESSED: " << keyValue << std::endl;
    }
}

void CardFlashFrame::OnGameDone(wxCommandEvent& event) {
    book_->SetSelection(0);
}

}