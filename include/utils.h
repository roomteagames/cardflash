#ifndef CDFL_UTILS_H
#define CDFL_UTILS_H

#include <string>
#include <rapidjson/document.h>

namespace cdfl {

namespace utils {

rapidjson::Document ReadJSONFile(const std::string& filename);

}

}

#endif /* CDFL_UTILS_H */