#ifndef CDFL_CONSTANTS_H
#define CDFL_CONSTANTS_H

#include <wx/wxprec.h>
#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif

namespace cdfl {

// Definition of custom event ids
extern int cfID_Hello;
extern int cfBUTTON_Start;
extern int cfBUTTON_Language;
extern int cfBUTTON_StartGame;
extern int cfBUTTON_Flip;
extern int cfBUTTON_Advance;
extern int cfBUTTON_Correct;
extern int cfBUTTON_Incorrect;
extern int cfBUTTON_Done;
extern int cfSELECT_Mode;
extern int cfSELECT_Chapter;
extern int cfSELECT_Direction;
extern int cfSELECT_Hint;
extern int cfSELECT_Eval;

// Modes
extern const wxString MODE_FULL;
extern const wxString MODE_CHAPTER;
extern const wxString MODE_VOCAB;

// Directions
extern const wxString DIR_FRONT_TO_BACK;
extern const wxString DIR_BACK_TO_FRONT;

// Toggle
extern const wxString TOGGLE_OFF;
extern const wxString TOGGLE_ON;

// Hint Type
extern const wxString HINT_FRONT;
extern const wxString HINT_BACK;

}

#endif /* CDFL_CONSTANTS_H */