#ifndef CDFL_GAME_H
#define CDFL_GAME_H

#include <string>
#include <vector>
#include <wx/wx.h>
#include <rapidjson/document.h>
#include "model.h"

namespace cdfl {

struct Game {
private:
    void create_chapters_list();

public:
    rapidjson::Document resource;
    unsigned short num_chapters;
    unsigned short num_correct;
    ModeSelection settings;
    wxString* chapters;
    std::vector<Card> cards;
    bool error;

    // state representation
    int card_index = 0;
    Card* current_card = nullptr;
    bool answer_phase = false;

    Game(const std::string& resource_path);
    ~Game();

    // Setup functions
    void Setup();
    void ClearCards();
    void AppendCards();
    void ShuffleCards();

    // Gameplay functions
    bool AdvanceCard(bool is_correct=false);
    void ShowAnswer();
    void FlipCard();
    wxString GetInfoText();
    wxString GetChallengeText();
    wxString GetAnswerText();
    wxString GetLabelText();
    wxString GetHintText();
    wxString GetCorrectText();
    wxString GetPercentText();
    bool IsFrontDisplayed();
    bool IsBackDisplayed();
    float GetEvalPercent();
};

}

#endif /* CDFL_GAME_H */