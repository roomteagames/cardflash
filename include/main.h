#ifndef CDFL_MAIN_H
#define CDFL_MAIN_H

#include <wx/wxprec.h>
#include <string>
#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif

namespace cdfl {

class CardFlashApp : public wxApp {
public:
    virtual bool OnInit();
};

}

#endif /* CDFL_MAIN_H */