#ifndef CDFL_MODEL_H
#define CDFL_MODEL_H

#include <string>
#include <iostream>
#include <wx/wx.h>

#include "constants.h"

namespace cdfl {

struct Chapter {
    unsigned int index = 0;
    std::string title = "Test";
    
    Chapter() {}
    Chapter(unsigned int, const std::string&);
    friend std::ostream& operator<< (std::ostream& out, const Chapter& obj);
};

struct ModeSelection {
    wxString mode = MODE_FULL;
    Chapter chapter = {0, "Chapter 1"};
    wxString hint = TOGGLE_OFF;
    wxString direction = DIR_FRONT_TO_BACK;
    wxString evaluate = TOGGLE_OFF;

    ModeSelection() {}
    ModeSelection(const wxString&, Chapter, const wxString&, const wxString&, const wxString&);
    friend std::ostream& operator<< (std::ostream& out, const ModeSelection& obj);
};

struct Card {
    wxString front;
    wxString back;
    wxString hint;
    bool correct;

    Card(const wxString& _front, const wxString& _back, const wxString& _hint, bool _correct = false);
    friend std::ostream& operator<< (std::ostream& out, const Card& obj);
};

}

#endif /* CDFL_MODEL_H */