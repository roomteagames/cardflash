#ifndef CDFL_FRAME_H
#define CDFL_FRAME_H

#include <wx/wxprec.h>
#include <wx/simplebook.h>
#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif

#include "model.h"
#include "game.h"

namespace cdfl {

class CardFlashFrame : public wxFrame {
public:
    CardFlashFrame(const wxString& title, const wxPoint& pos, const std::string& resource_path);

private:
    Game game_;

    wxPanel* start_page_;
    wxPanel* mode_page_;
    wxPanel* action_page_;
    wxPanel* language_page_;
    wxPanel* game_page_;
    wxPanel* results_page_;

    wxSimplebook* book_;

    // Mode selection fields
    wxPanel* chapter_pane_;
    wxPanel* direction_pane_;
    wxPanel* hint_pane_;
    wxPanel* eval_pane_;

    // Dynamic Game elements
    wxStaticText* challenge_text_;
    wxStaticText* info_text_;
    wxPanel* game_hint_pane_;
    wxStaticText* hint_text_;
    wxButton* advance_button_;
    wxPanel* game_eval_pane_;
    wxStaticText* correct_text_;
    wxStaticText* percent_text_;

    ModeSelection mode_settings_;

    // Fonts
    wxFont title_font_;
    wxFont secondary_font_;

    void BuildStartPage();
    void BuildModePage();
    void BuildGamePage();
    void BuildResultsPage();

    void OnStart(wxCommandEvent& event);
    void OnHello(wxCommandEvent& event);
    void OnExit(wxCommandEvent& event);
    void OnAbout(wxCommandEvent& event);
    void OnModeSelection(wxCommandEvent& event);
    void OnChapterSelection(wxCommandEvent& event);
    void OnDirectionSelection(wxCommandEvent& event);
    void OnHintSelection(wxCommandEvent& event);
    void OnEvalSelection(wxCommandEvent& event);
    void OnStartGame(wxCommandEvent& event);
    void OnCardFlip(wxCommandEvent& event);
    void OnCardAdvance(wxCommandEvent& event);
    void OnCardCorrect(wxCommandEvent& event);
    void OnGameDone(wxCommandEvent& event);
    void OnKeyPress(wxKeyEvent& event);

    void AdvanceCard(bool is_correct = false);
    void HandleCardFinish();

    wxDECLARE_EVENT_TABLE();
};

}

#endif /* CDFL_FRAME_H */