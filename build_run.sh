#!/bin/bash -e

# Clean build directory and change to it
[[ -d build ]] && rm -rf build
mkdir build
cd build

# Build project and run
cmake ..
make
./Cardflash